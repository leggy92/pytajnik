prostokąt - rectangle
wzorzec - pattern
falka - wavelet
soczewka - lens
telewizja przemysłowa - closed-circuit tv
podczerwień - infrared
mapa ciepła - heat map
powielanie - amplification
wiązanie - bond
częsteczka - molecule
światłocień - chiaroscuro
rampa dla niepełnosprawnych - wheelchair ramp
żelbet - reinforced concrete
przekrój - section
rzut - plan
zagospodarowanie przestrzenne - site plan
elewacja - elevation
paliwo - fuel
odnawialna energia - renewable energy
