praca - labor
manipulator - robotic arm
złożony z - made up of
przegub - joint
silnik krokowy - step motor
czujnik ruchu - motion sensor
przypominać coś - resemble
łokieć - elbow
mocować - mount
stopnień swobody - degree of freedom
obracać się - pivot
wyposażyć - outfit
złapać - grasp
czujnik ciśnienia - pressure sensor
palnik spawalniczy - blowtorch
wiertło - drill
przechowywać - store
linia montarzowa - assembly line
chwytak - gripper
przylepny - adhesive
spawanie - welding
przyssawki - suction cups